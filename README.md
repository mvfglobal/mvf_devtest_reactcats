# MVF Developer Tests
## React (cats gallery)

Thanks for your interest in a developer role at MVF.

We have a simple project which we would like you to take a look at in your own time.

You can spend as much or as little time on it as you wish, but if we have asked you to take a look at this, we would usually expect to receive a response in a 3-4 days.

The task is to build a React app based on the data in this repository. We would like you to attempt this challenge using React, but aside from that you can use any language(s) you are familiar with, using any other libraries, modules or frameworks you feel appropriate. See it as a opportunity to showcase your professional software engineering skills.

How would you implement this? Fork our repo and let us see your ideas!

### Challenge
Implement the stories in userStories.md to build a photo feed gallery.

If you wish, you can refer to wireframes wireframe.png and bonus_stories_wireframe.png to guide you.

The data describing the gallery contents is located in photoCardsData.json

Your app should be able to take any json in a similar format and display the relevant gallery.

### Resources
You don't have to use these, but you may find them helpful:

Create React App - [https://github.com/facebook/create-react-app](https://github.com/facebook/create-react-app)
React Starter Kit - [https://github.com/rm-bergmann/app-starter-kit](https://github.com/rm-bergmann/app-starter-kit)

---
### MVF
Do you want to work with the Smartest Tech and the Sharpest Minds? Apply at: http://www.mvfglobal.com/vacancies